package com.example.webservicegroups;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface GroupRepository extends JpaRepository<GroupEntity, String> {
    GroupEntity findGroupByName(String name);
}
