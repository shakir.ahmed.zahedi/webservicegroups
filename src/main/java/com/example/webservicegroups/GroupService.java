package com.example.webservicegroups;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
@Service
@AllArgsConstructor
public class GroupService {
    @Autowired
    GroupRepository groupRepository;

    public GroupEntity addNewGroup(CreateGroup createGroup) {
        GroupEntity groupEntity = new GroupEntity(
                UUID.randomUUID().toString(),
                createGroup.getName()
        );
        groupRepository.save(groupEntity);
        return groupEntity;
    }

    public List<GroupEntity> getAll() {
        return groupRepository.findAll();
    }

    public GroupEntity deleteUser(String id) {
        GroupEntity groupEntity = groupRepository.getById(id);
        groupRepository.delete(groupEntity);
        return  groupEntity;
    }

    public GroupEntity updateGroup(String id, UpdateGroup updateGroup) {
        GroupEntity groupEntity = groupRepository.getById(id);
        groupEntity.setName(updateGroup.getName());
        groupRepository.save(groupEntity);
        return groupEntity;
    }

    public GroupEntity findGroupByName(String name) {
        GroupEntity groupEntity = groupRepository.findGroupByName(name);
        return groupEntity;
    }
}
