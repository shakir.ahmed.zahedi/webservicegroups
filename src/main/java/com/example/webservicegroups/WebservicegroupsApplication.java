package com.example.webservicegroups;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@SpringBootApplication
public class WebservicegroupsApplication {
    public static void main(String[] args) {
        SpringApplication.run(WebservicegroupsApplication.class, args);
    }
}
