package com.example.webservicegroups;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@Table(name = "groups")
@Entity
@NoArgsConstructor
public class GroupEntity {
    @Id
    String id;
    String name;
    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    List<String> userIds;

    @JsonCreator
    public GroupEntity(@JsonProperty("id") String id,@JsonProperty("name") String name) {
        this.id = id;
        this.name = name;
        this.userIds = new ArrayList<>();
    }

}
