package com.example.webservicegroups;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@NoArgsConstructor
public class CreateGroup {
    String name;

    @JsonCreator
    public CreateGroup(@JsonProperty String name) {
        this.name = name;
    }

}
