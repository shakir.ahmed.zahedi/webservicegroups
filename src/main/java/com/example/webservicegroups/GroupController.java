package com.example.webservicegroups;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
@RestController
@RequestMapping("/groups")
@AllArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class GroupController {
    GroupService groupService;

    @PostMapping("/add")
    public GroupEntity addNewUser(@RequestBody CreateGroup createGroup) throws IOException {
        GroupEntity groupEntity = groupService.addNewGroup(createGroup);
        return groupEntity;
    }

    @GetMapping()
    public List<GroupEntity> all(){
        return groupService.getAll().stream()
                .collect(Collectors.toList());
    }
    @DeleteMapping("/{id}")
    public GroupEntity deleteGroup(@PathVariable("id") String id){
        GroupEntity groupEntity = groupService.deleteUser(id);
        return  groupEntity;
    }

    @PutMapping("/{id}")
    public GroupEntity updateGroupName(@PathVariable("id") String id,@RequestBody UpdateGroup updateGroup){
        GroupEntity groupEntity = groupService.updateGroup(id,updateGroup);
        return  groupEntity;
    }

    @GetMapping("/{name}")
    public GroupEntity findGroupByName(@PathVariable("name") String name) {
        return groupService.findGroupByName(name);
    }


}
