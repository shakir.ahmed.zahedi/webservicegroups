package com.example.webservicegroups;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@NoArgsConstructor
public class UpdateGroup {
    String name;

    @JsonCreator
    public UpdateGroup(@JsonProperty String name) {
        this.name = name;
    }
}
